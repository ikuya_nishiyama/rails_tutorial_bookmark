class PostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: [:edit, :update, :destroy]
  
  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:success] = "ブックマークが登録されました！"
      redirect_to root_url
    else
      @feed_items = current_user.feed.paginate(page: params[:page])
      render "static_pages/home"
    end
  end
  
  def destroy
    @post.destroy
    flash[:success] = "ブックマークを削除しました"
    redirect_to request.referrer || root_url
  end
  
  def edit
    @feed_items = current_user.feed.paginate(page: params[:page])
  end
  
  def update
    if @post.update_attributes(post_params)
      flash[:success] = "ブックマークを更新しました"
      redirect_to root_url
    else
      @feed_items = current_user.feed.paginate(page: params[:page])
      render "edit"
    end
  end
  
  private 
    
    def post_params
      params.require(:post).permit(:chapter, :title, :link, :memo)
    end
    
    def correct_user
      @post = current_user.posts.find_by(id: params[:id])
      redirect_to root_url if @post.nil?
    end
end
