class SessionsController < ApplicationController
  def new
  end
  
  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # アカウントが有効化されているか確認
      if user.activated?
        log_in user
        # 永続セッションのために記憶トークンを生成し、データベースに保存
        # ブラウザのCookiesに暗号化IDと暗号化された記憶トークンを保存
        params[:session][:remember_me] == "1" ? remember(user) : forget(user)
        redirect_back_or(user)
      else
        message = "アカウントが有効化されていません。"
        message += "お送りしたメールの有効化リンクを押してください。"
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = "メールアドレスかパスワードが間違っています。"
      render "new"  
    end
  end
  
  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
