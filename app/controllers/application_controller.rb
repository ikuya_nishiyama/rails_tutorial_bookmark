class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include SessionsHelper
  
  private
  
    # ログイン済みユーザーかどうか確認(未ログインならログイン画面に飛ばす)
    def logged_in_user
      unless logged_in?
        # URLを覚えておく
        store_location
        flash[:danger] = "ログインしてください"
        redirect_to login_url
      end
    end
end
