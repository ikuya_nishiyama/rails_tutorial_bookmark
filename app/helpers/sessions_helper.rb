module SessionsHelper
  
  # 渡されたユーザーでログインする
  def log_in(user)
    session[:user_id] = user.id
  end
  
  # ユーザーのセッションを永続的にする(Userモデルにrememberメソッド（クラスメソッドあり）)
  def remember(user)
    # Userモデルのrememberメソッドを使って、
    # データベースのremember_digestカラムを保存
    user.remember
    # ブラウザのCookiesに暗号化IDを保存(permanentで永続化)
    cookies.permanent.signed[:user_id] = user.id
    # ブラウザのCookiesにremember_tokenを保存(permanentで永続化)
    cookies.permanent[:remember_token] = user.remember_token
  end
  
  # 渡されたユーザーがログイン済みユーザーであればtrueを返す
  def current_user?(user)
    user == current_user
  end
  
  
  # セッションとクッキーを確認し、現在ログイン中のユーザーを返す（いる場合）
  def current_user
    # セッションにユーザーIDが記録されているか確認
    if session[:user_id]
      @current_user ||= User.find_by(id: session[:user_id])
    # クッキーにユーザーIDが記録されているか確認
    # クッキーの場合は、記憶化トークンも保存されているため、データベースと照合する
    elsif cookies.signed[:user_id]
      user = User.find_by(id: cookies.signed[:user_id])
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end
  
  # ユーザーがログインしていればtrue、その他ならfalseを返す
  def logged_in?
    !current_user.nil?
  end
  
  # 永続的セッションを破棄する（cookiesに保存されたIDとトークンを削除）
  def forget(user)
    # Userモデルのforgetメソッドを使用し、データベースの記憶化ダイジェストを削除
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
  
  # 現在のユーザーをログアウトする
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end
  
  # 記憶したURL（もしくはデフォルト値）にリダイレクト
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end
  
  # アクセスしようとしたURLを覚えておく
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
  
end
