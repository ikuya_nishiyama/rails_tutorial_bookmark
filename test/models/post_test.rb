require 'test_helper'

class PostTest < ActiveSupport::TestCase
  
  def setup
    @user = users(:michael)
    @post = @user.posts.build(chapter: "13", title: "heroku",
                              link: "https://heroku.com", memo: "herokuのサイト")
  end
  
  test "有効なデータ" do
    assert @post.valid?
  end
  
  test "ユーザーIDが存在しているはず" do
    @post.user_id = nil
    assert_not @post.valid?
  end
  
  test "リンクは必ず存在する" do
    @post.link = ""
    assert_not @post.valid?
  end
  
  test "投稿が新しいものの順に表示" do
    assert_equal posts(:most_recent), Post.first
  end
  
end
