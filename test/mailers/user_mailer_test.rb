require 'test_helper'

class UserMailerTest < ActionMailer::TestCase
  test "アカウントの有効化" do
    user = users(:michael)
    user.activation_token = User.new_token
    # UserMailerクラスのaccount_activation(user)メソッド
    mail = UserMailer.account_activation(user)
    assert_equal "アカウントの有効化", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_match user.name,               mail.text_part.body.to_s.encode("UTF-8")
    assert_match user.activation_token,   mail.text_part.body.to_s.encode("UTF-8")
    assert_match CGI.escape(user.email),  mail.text_part.body.to_s.encode("UTF-8")
  end
  
  test "パスワードの再設定" do
    user = users(:michael)
    user.reset_token = User.new_token
    # UserMailerクラスのpassword_reset(user)メソッド（メールを送信するメソッド）
    mail = UserMailer.password_reset(user)
    assert_equal "パスワードの再設定", mail.subject
    assert_equal [user.email], mail.to
    assert_equal ["noreply@example.com"], mail.from
    assert_match user.reset_token,   mail.text_part.body.to_s.encode("UTF-8")
    assert_match CGI.escape(user.email),  mail.text_part.body.to_s.encode("UTF-8")
  end
end
