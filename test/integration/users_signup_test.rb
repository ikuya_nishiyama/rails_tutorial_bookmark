require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  
  def setup
    ActionMailer::Base.deliveries.clear
  end
  
  test "ログイン失敗時：エラーが表示され、ログインページが表示される" do
    get signup_path
    assert_no_difference "User.count" do
      post signup_path, params: { user: { name: "",
                                         email: "user@inbalid",
                                         password: "foo",
                                         password_confirmation: "bar" } }
    end
    assert_select "div#error_explanation"
    assert_select "div.alert"
    assert_template "users/new"  
    assert_select 'form[action="/signup"]'
  end
  
  test "ログイン成功時：アカウントが有効化された情報で適正な情報を入力" do
    get signup_path
    assert_difference "User.count", 1 do
      post users_path, params: { user: { name: "Example User",
                                          email: "user@example.com",
                                          password: "password",
                                          password_confirmation: "password" } }
    end
    assert_equal 1, ActionMailer::Base.deliveries.size
    user = assigns(:user)
    assert_not user.activated?
    #有効化していな状態でログインしてみる
    log_in_as(user)
    assert_not is_logged_in?
    # 有効化トークンが不正な場合
    get edit_account_activation_path("invalid token", email: user.email)
    assert_not is_logged_in?
    # トークンは正しいが、メールアドレスが無効な場合
    get edit_account_activation_path(user.activation_token, email: "wrong")
    assert_not is_logged_in?
    # 有効化トークンが正しい場合
    get edit_account_activation_path(user.activation_token, email: user.email)
    assert user.reload.activated?    
    follow_redirect!
    assert_template "users/show"
    assert_not flash.empty?
    # assert is_logged_in?
  end
  

end
