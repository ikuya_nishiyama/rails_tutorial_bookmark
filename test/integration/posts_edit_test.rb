require 'test_helper'

class PostsEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
    @post = posts(:heroku)
  end

  test "ブックマークの編集" do
    log_in_as(@user)
    get edit_post_path(@post)
    assert_template 'posts/edit'
    
    # リンクを入力せずの更新は無効
    patch post_path(@post), params: { post: { link: "" } }
    assert_template 'posts/edit'
    
    # 有効な入力
    patch post_path(@post), params: { post: { chapter: "1.0.0",
                                              title: "rails tutorial",
                                              link: "https://rails.com",
                                              memo: "rails tutorialについて" } }
    assert_redirected_to root_url
  end
end
