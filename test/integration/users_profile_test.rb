require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper
  
  def setup
    @user = users(:michael)
  end
  
  test "プロフィールの描写" do
    get user_path(@user)
    assert_template "users/show"
    assert_select "title", full_title(@user.name)
    assert_select "h1", tect: @user.name
    assert_select "div.pagination"
    @user.posts.paginate(page: 1).each do |post|
      assert_match post.link, response.body
    end
  end
end
