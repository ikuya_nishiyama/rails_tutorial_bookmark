require 'test_helper'

class PostsInterfaceTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end
  
  test "投稿のインターフェイス" do
    log_in_as(@user)
    get root_url
    assert_template "static_pages/home"
    assert_select "div.pagination"
    
    # 無効なフォームの送信
    assert_no_difference "Post.count" do
      post posts_path, params: { post: { link: "" } }
    end
    assert_select "div#error_explanation"
    
    # 有効な送信
    chapter = "1.0.0"
    title = "heroku"
    link = "https://heroku.com"
    memo = "herokuのホームページ"
    assert_difference "Post.count", 1 do
      post posts_path, params: { post: { chapter: chapter,
                                         title: title,
                                         link: link,
                                         memo: memo } }
    end
    assert_redirected_to root_url
    follow_redirect!
    assert_match link, response.body
    
    # 投稿を削除する
    assert_select "a", text: "ブックマークを削除"
    first_post = @user.posts.paginate(page: 1).first
    assert_difference "Post.count", -1 do
      delete post_path(first_post)
    end
    
    # 違うユーザーのプロフィールにアクセス(削除リンクがないことを確認)
    get user_path(users(:archer))
    assert_select "a", text: "ブックマークを削除", count: 0
  end
end
