require 'test_helper'

class PostsControllerTest < ActionDispatch::IntegrationTest

  def setup
    @post = posts(:heroku)
  end
  
  test "未ログイン時はcreateできずredirectされる" do
    assert_no_difference "Post.count" do
      post posts_path, params: { post: { link: "https://heroku.com" } }
    end
    assert_redirected_to login_url
  end
  
  test "未ログイン時はdestroyできずredirectされる" do
    assert_no_difference "Post.count" do
      delete post_path(@post)
    end
    assert_redirected_to login_url
  end
  
  test "間違ったユーザーによるブックマーク削除は無効" do
    log_in_as(users(:michael))
    post = posts(:archer1)
    assert_no_difference "Post.count" do
      delete post_path(post)
    end
    assert_redirected_to root_url
  end
end
