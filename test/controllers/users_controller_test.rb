require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  
  def setup
    @user = users(:michael)
    @other_user = users(:archer)
  end

  test "ログインしていない時にindexしようとするとリダイレクトされる" do
    get users_path
    assert_redirected_to login_url
  end
  
  test "should get new" do
    get signup_path
    assert_response :success
  end

  test "ログインしていない時にeditしようとするとリダイレクトされる" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "ログインしていない時にupdateしようとするとリダイレクトされる" do
    patch user_path(@user), params: { user: { name: @user.name,
                                              email: @user.email} }
    assert_not flash.empty?
    assert_redirected_to login_url
  end
  
  test "admin属性を変更することは許可されていない" do
    log_in_as(@other_user)
    assert_not @other_user.admin?
    patch user_path(@other_user), params: { 
                                  user: { password: "password",
                                          password_confirmation: "password",
                                          admin: true } }
    assert_not @other_user.reload.admin?
  end
  
  test "ログインしていない時にdestroyしようとするとリダイレクトされる" do
    assert_no_difference "User.count" do
      delete user_path(@user)
    end
    assert_redirected_to login_url
  end
  
  test "非管理者でログインしている時にdestroyしようとするとリダイレクトされる" do
    log_in_as(@other_user)
    assert_no_difference "User.count" do
      delete user_path(@user)
    end
    assert_redirected_to root_url
  end
end
