# 本番用アカウント
User.create!(name: "RTbookmark",
             email: "rails.tutorial.bookmark@gmail.com",
             password: "password",
             password_confirmation: "password",
             activated: true,
             activated_at: Time.zone.now)
           
# ユーザーを大量に作成  
99.times do |n|
  name = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name: name,
               email: email,
               password: password,
               password_confirmation: password,
               activated: true,
               activated_at: Time.zone.now)
end

# 本番用ブックマーク一覧
user = User.find_by(name: "RTbookmark")
user.posts.create!(chapter: "1.5.1",
                   title: "heroku login --interactive",
                   link: "https://qiita.com/shosho/items/41183091769a8693a8a5",
                   memo: "--interactiveの意味")
user.posts.create!(chapter: "3.2",
                   title: "ローカルブランチの削除方法",
                   link: "https://qiita.com/hogeta_/items/33d2334c9b1919bd5120",
                   memo: "git branch --delete [ブランチ名]\ngit branch -d [ブランチ名]")
user.posts.create!(chapter: "3.2",
                   title: "Git checkout とは",
                   link: "https://www.sejuku.net/blog/71457",
                   memo: "「ブランチを切り替えたいとき」に使用するコマンド")
user.posts.create!(chapter: "1.4.3",
                   title: "Git originとは",
                   link: "https://reasonable-code.com/git-origin/",
                   memo: "リモートリポジトリのアクセス先に対してGitがデフォルトでつける名前")
user.posts.create!(chapter: "3.2.1",
                   title: "CRUDとREST",
                   link: "https://suginoy.hatenadiary.org/entry/20120109/p1")
user.posts.create!(chapter: "3.3",
                   title: "TDD(テスト駆動開発)とは",
                   link: "https://www.valtes.co.jp/qbookplus/1069",
                   memo: "プログラムの実装前にテストコードを書き（テストファースト）、そのテストコードに適合するように実装とリファクタリングを進めていく方法")
user.posts.create!(chapter: "3.3",
                   title: "テストの種類について",
                   link: "https://qiita.com/duka/items/2d724ea2226984cb544f",
                   memo: "単体テスト、機能テスト、統合テスト")
user.posts.create!(chapter: "3.6.2",
                   title: "springとは",
                   link: "https://ruby-rails.hatenadiary.com/entry/20141026/1414289421",
                   memo: "Rails4.1から標準で付属するようになったアプリケーションプリローダー。
                          事前にバックグラウンドでライブラリをロードしておくことで、その待ち時間を短くするものがアプリケーションプリローダー。")
user.posts.create!(chapter: "4.4.5",
                   title: "attrbute_accessorとは",
                   link: "https://teratail.com/questions/94734",
                   memo: "attr_accessorは、引数で指定した名前のsetter/getterを自動で定義してくれる命令")
user.posts.create!(chapter: "5.1.2",
                   title: "Sass, Scss, Lessの違い",
                   link: "http://brriant.hatenablog.com/entry/2015/09/22/145413")
user.posts.create!(chapter: "5.2.1",
                   title: "Sass, Lessの違い",
                   link: "https://techacademy.jp/magazine/19498")
user.posts.create!(chapter: "5.3.4",
                   title: "Rakeタスクとは",
                   link: "https://www.ryotaku.com/entry/2019/04/17/Rake%E3%82%BF%E3%82%B9%E3%82%AF%E3%81%A8%E3%81%AF",
                   memo: "行いたい処理をターミナルなどのコマンドライン上から実行できる機能の一つ")
user.posts.create!(chapter: "6.1",
                   title: "Active Recordとは",
                   link: "https://qiita.com/ryokky59/items/a1d0b4e86bacbd7ef6e8",
                   memo: "RailsにはModelにActiveRecordが適用されているおかげで、Rubyを用いてDBからデータを探したり、持ってきたりすることができる")
user.posts.create!(chapter: "6.2.5",
                   title: "バリデーションについて",
                   link: "https://web-camp.io/magazine/archives/16817")
user.posts.create!(chapter: "6.2.5",
                   title: "データベースのインデックス",
                   link: "http://www.hi-ho.ne.jp/tsumiki/doc_1.html",
                   memo: "テーブルに格納されているデータを高速に取り出す為の仕組み")
user.posts.create!(chapter: "6.3.4",
                   title: "6.3の演習",
                   link: "https://qiita.com/mochikichi321/items/32fa0d39e0fbeae79874")
user.posts.create!(chapter: "7.1.1",
                   title: "デバッグとは",
                   link: "https://wa3.i-3-i.info/word1419.html",
                   memo: "不具合の原因を探して直すこと")
user.posts.create!(chapter: "7.1.4",
                   title: "キーワード引数とオプション引数の違い",
                   link: "https://rails-study.net/keyword-option/",
                   memo: "オプション引数は、key自体も自由に渡せる引数のこと")
user.posts.create!(chapter: "7.3.2",
                   title: "マスアサインメント機能と脆弱性",
                   link: "https://qiita.com/tbpgr/items/63028f5e19a2d1617e40",
                   memo: "DBの複数のカラムを一括で指定できる機能のこと。Strong Parametersを使って対策。")
user.posts.create!(chapter: "7.3.3",
                   title: "エラーメッセージの日本語化",
                   link: "https://qiita.com/Ushinji/items/242bfba84df7a5a67d5b")
user.posts.create!(chapter: "7.3.4",
                   title: "7.3.4演習",
                   link: "https://www.yokoyan.net/entry/2017/09/02/152358")
user.posts.create!(chapter: "7.4.4",
                   title: "content_tagについて",
                   link: "https://shinmedia20.com/rails-content-tag")
user.posts.create!(chapter: "9.1.1",
                   title: "セッションとクッキーの違い①",
                   link: "http://ohs30359.hatenablog.com/entry/2015/09/04/235027",
                   memo: "ログイン情報を格納する場所が異なる")
user.posts.create!(chapter: "9.1.1",
                   title: "セッションとクッキーの違い②",
                   link: "https://qiita.com/hot_study_man/items/147f8b767b4135fe6fe4")
user.posts.create!(chapter: "11.1.1",
                   title: "11.1.1演習",
                   link: "https://logicoffee.hatenablog.com/entry/2018/07/02/183907")                   
user.posts.create!(chapter: "11.3.3",
                   title: "update_attributesとupdate_columnsの違い",
                   link: "https://sakurawi.hateblo.jp/entry/update_attributes",
                   memo: "バリデーションとコールバックが関連する模様")             
user.posts.create!(chapter: "13.1.4",
                   title: "Proc, ラムダとは",
                   link: "https://qiita.com/y4u0t2a1r0/items/7504bf99563001cab503",
                   memo: "proc：ブロックをオブジェクトに変換する")
user.posts.create!(chapter: "13.3.2",
                   title: "form_forのfield",
                   link: "https://www.sejuku.net/blog/13163")
user.posts.create!(chapter: "13.3.3",
                   title: "SQLインジェクション",
                   link: "https://www.kagoya.jp/howto/network/sql-injection/3",
                   memo: "不正な「SQL」の命令を攻撃対象のウェブサイトに「注入する（インジェクションする）」のが、SQLインジェクション")




